package config

import (
	"github.com/charmbracelet/log"
	"github.com/spf13/viper"
)

// Writes the changes made to current config to the actual config file
func WriteToConfig(vp *viper.Viper) error {
	log.Debug("Writing to config file...")

	err := vp.WriteConfig()
	if err != nil {
		log.Error("Error writing to config file", "err", err)
		return err
	}

	log.Info("Successfully wrote to config file")
	return nil
}

// Reads in configuration file to runtime
func LoadConfig(vp *viper.Viper) (config *viper.Viper, err error) {
	// Load the config
	err = vp.ReadInConfig()
	if err != nil {
		log.Error("Error reading config", "err", err)
		return vp, err
	}

	return vp, nil
}

// Initializes a configuration file with Name of type Type (i.e config.json), by default it looks for the
// configuration in the following order: 1. Current Directory 2. $XDG_CONFIG_DIR 3. $HOME/TaskAlert
func InitConf(Name string, Type string) (config *viper.Viper, err error) {
	vp := viper.New()

	// Config name
	vp.SetConfigName(Name)
	vp.SetConfigType(Type)

	// Config locations
	vp.AddConfigPath(".")
	vp.AddConfigPath("$HOME/.config/TaskAlert/")
	vp.AddConfigPath("$HOME/TaskAlert/")

	vp, err = LoadConfig(vp)
	if err != nil {
		return vp, err
	}

	return vp, nil
}
