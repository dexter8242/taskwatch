package main

import (
	"flag"

	"github.com/charmbracelet/log"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	c "gitlab.com/dexter8242/taskwatch/internal"
)

// Initializes the application with name and version and writes it to the
// configuration file, returns an error if it fails
func initApp(vp *viper.Viper) error {
	log.Debug("Setting app name variable...")
	vp.Set("appName", "TaskWatch")

	appName := vp.GetString("appName")
	log.Info("App Name", "appName", appName)

	log.Debug("Setting application version...")
	vp.Set("version", "v1")

	version := vp.GetString("version")
	log.Info("App Version", "version", version)

	err := c.WriteToConfig(vp)
	if err != nil {
		return err
	}

	return nil
}

// Parses the --log-level flag and sets the log.setLogLevel to the specified value (debug, info etc.).
func setLogLevel() error {
	// Define the loglevel
	var logLevel string
	flag.StringVar(&logLevel, "log-level", "info", "Set the log level (debug, info, warn, error, fatal)")

	// Parse the flags
	pflag.CommandLine.AddGoFlagSet(flag.CommandLine)
	pflag.Parse()

	// Bind the flag value to viper configuration
	err := viper.BindPFlag("log.level", pflag.CommandLine.Lookup("log-level"))
	if err != nil {
		return err
	}

	// Retrieve log level value from viper configuration
	logLevel = viper.GetString("log.level")

	// Match specified log level
	switch logLevel {
	case "debug":
		log.SetLevel(log.DebugLevel)
	case "info":
		log.SetLevel(log.InfoLevel)
	case "warn":
		log.SetLevel(log.WarnLevel)
	case "error":
		log.SetLevel(log.ErrorLevel)
	case "fatal":
		log.SetLevel(log.FatalLevel)
	}

	// All good, return nil
	return nil
}

func main() {
	// Parse user defined log level and set it
	err := setLogLevel()
	if err != nil {
		log.Fatal("Failed to set log level", "err", err)
	}

	log.Debug("Starting program...")

	// Initialize config file
	log.Debug("Initializing configuration...")
	vp, err := c.InitConf("config", "json")
	if err != nil {
		log.Fatal("Error initializing configuration", "err", err)
	}

	// Set application variables in config file
	log.Debug("Initializing application...")

	err = initApp(vp)
	if err != nil {
		log.Error("Error initializing application", "err", err)
	}

}
